FROM python:3.8-slim

WORKDIR /app

COPY app.py /app/

EXPOSE 80


RUN rm -rf /var/lib/apt/lists/* /root/.cache/pip

CMD ["python", "app.py"]
